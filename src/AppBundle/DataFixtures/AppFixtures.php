<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\User;
use AppBundle\Entity\Vet;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /**
     * @var UserManagerInterface
     */
    private $userManager;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * AppFixtures constructor.
     * @param UserManagerInterface $userManager
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserManagerInterface $userManager, UserPasswordEncoderInterface $encoder)
    {
        $this->userManager = $userManager;
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $arr = [
            'user' => [
                'username' => 'fulano',
                'firstName' => 'Fulano',
                'lastName' => 'de Tal',
            ],
            'vet' => [
                'username' => 'vet',
                'firstName' => 'Veterinário',
                'lastName' => 'da Silva',
                'specialty' => 'Gatos',
                'bio' => 'Lorem ipsum dolor sit amet.',
            ],
        ];

        foreach ($arr as $type => $data) {
            /** @var User $user */
            $user = $this->userManager->createUser();
            $user->setFirstName($data['firstName']);
            $user->setLastName($data['lastName']);
            $user->setUsername($data['username']);
            $user->setEmail("{$data['username']}@email.co");
            $user->setEnabled(true);
            $user->setSalt(md5(uniqid()));

            $password = $this->encoder->encodePassword($user, '12345');
            $user->setPassword($password);

            if ($type === 'vet') {
                $vet = new Vet();
                $vet->setSpecialty($data['specialty']);
                $vet->setBio($data['bio']);
                $user->setVet($vet);
                $manager->persist($vet);
            }

            $this->userManager->updateUser($user);
        }
        $manager->flush();
    }
}
