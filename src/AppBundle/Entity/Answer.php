<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Answer
 *
 * @ORM\Table(name="answer")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AnswerRepository")
 */
class Answer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="answer", type="text")
     */
    private $answer;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="helpful", type="boolean", nullable=true)
     */
    private $helpful;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Question", inversedBy="answers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $question;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Vet", inversedBy="answers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $vet;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set answer.
     *
     * @param string $answer
     *
     * @return Answer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer.
     *
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set helpful.
     *
     * @param bool|null $helpful
     *
     * @return Answer
     */
    public function setHelpful($helpful = null)
    {
        $this->helpful = $helpful;

        return $this;
    }

    /**
     * Get helpful.
     *
     * @return bool|null
     */
    public function getHelpful()
    {
        return $this->helpful;
    }

    /**
     * Set question.
     *
     * @param \AppBundle\Entity\Question $question
     *
     * @return Answer
     */
    public function setQuestion(\AppBundle\Entity\Question $question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question.
     *
     * @return \AppBundle\Entity\Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set vet.
     *
     * @param \AppBundle\Entity\Vet $vet
     *
     * @return Answer
     */
    public function setVet(\AppBundle\Entity\Vet $vet)
    {
        $this->vet = $vet;

        return $this;
    }

    /**
     * Get vet.
     *
     * @return \AppBundle\Entity\Vet
     */
    public function getVet()
    {
        return $this->vet;
    }
}
