<?php

namespace AppBundle\Api;

use Symfony\Component\HttpKernel\Exception\HttpException;

class ApiException extends HttpException
{
    /**
     * @var ApiError
     */
    private $apiError;

    public function getApiError()
    {
        return $this->apiError;
    }

    public function __construct(ApiError $apiError, \Exception $previous = null, array $headers = [], $code = 0)
    {
        $this->apiError = $apiError;
        $statusCode = $apiError->getStatusCode();
        $message = '$apiProblem->getTitle()';

        parent::__construct($statusCode, $message, $previous, $headers, $code);
    }
}
