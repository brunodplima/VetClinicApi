<?php

namespace AppBundle\Api;

class ApiError
{
    private $statusCode;
    private $extraData = [];

    public function __construct($statusCode)
    {
        $this->statusCode = $statusCode;
    }

    public function toArray()
    {
        return array_merge(
            $this->extraData,
            [
                'status' => $this->statusCode,
            ]
        );
    }

    public function set($name, $value)
    {
        $this->extraData[$name] = $value;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }
}
