<?php

namespace AppBundle\Form;

use AppBundle\Entity\Vet;
use AppBundle\Model\VetModel;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VetModelType extends UserType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('user', UserType::class) // JSON precisaria ter mais de um nível
            ->add('firstName')
            ->add('lastName')
            ->add('specialty')
            ->add('bio')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => VetModel::class,
            'csrf_protection' => false,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'api_vet';
    }
}
