<?php

namespace AppBundle\EventListener;

use AppBundle\Api\ApiError;
use AppBundle\Api\ApiException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\KernelEvents;

class ApiExceptionSubscriber implements EventSubscriberInterface
{
    private $debug;

    public function __construct($debug)
    {
        $this->debug = $debug;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException'
        ];
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // only reply to api URLs
        $strpos = strpos($event->getRequest()->getPathInfo(), '/api/');
        if ($strpos === false) {
            return;
        }

        $exception = $event->getException();
        $statusCode = $exception instanceof HttpExceptionInterface ? $exception->getStatusCode() : 500;

        // allow server errors to be thrown
        if (intdiv($statusCode, 100) == 5 && $this->debug) {
            return;
        }

        if ($exception instanceof ApiException) {
            $apiProblem = $exception->getApiError();
        } else {
            $apiProblem = new ApiError($statusCode);
            if ($exception instanceof HttpExceptionInterface) {
                $apiProblem->set('detail', $exception->getMessage());
            }
        }

        $data = $apiProblem->toArray();
        $response = new JsonResponse(
            $data,
            $apiProblem->getStatusCode()
        );
        $response->headers->set('Content-Type', 'application/json');

        $event->setResponse($response);
    }
}
