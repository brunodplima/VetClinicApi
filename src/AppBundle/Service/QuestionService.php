<?php

namespace AppBundle\Service;

use AppBundle\Entity\Question;
use AppBundle\Entity\User;
use AppBundle\Model\QuestionModel;
use Doctrine\ORM\EntityManagerInterface;

class QuestionService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Retorna todas as perguntas registradas no sistema
     *
     * @return array
     */
    public function getAll(): array
    {
        $questions = $this->entityManager->getRepository(Question::class)->findAll();
        return $this->getModelCollection($questions);
    }

    /**
     * Retorna todas as perguntas feitas por um usuário
     *
     * @param User $user
     * @param bool $onlyPublic
     * @return array
     */
    public function getAllByUser(User $user, bool $onlyPublic = true): array
    {
        $questions = $this->entityManager->getRepository(Question::class)
            ->getAllByUser($user, $onlyPublic);
        return $this->getModelCollection($questions);
    }

    /**
     * @param array $questions
     * @return array
     */
    private function getModelCollection(array $questions): array
    {
        $all = [];
        foreach ($questions as $question) {
            $all[] = new QuestionModel($question);
        }
        return $all;
    }
}
