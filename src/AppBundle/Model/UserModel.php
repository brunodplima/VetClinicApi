<?php

namespace AppBundle\Model;

use AppBundle\Entity\User;

class UserModel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    public $firstName = null;

    /**
     * @var string
     */
    public $lastName = null;

    /**
     * @param User $user
     */
    public function __construct(User $user = null)
    {
        if ($user) {
            $this->id = $user->getId();
            $this->firstName = $user->getFirstName();
            $this->lastName = $user->getLastName();
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function updateUser(User $user)
    {
        $user->setFirstName($this->firstName);
        $user->setLastName($this->lastName);
    }
}
