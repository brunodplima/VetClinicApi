<?php

namespace AppBundle\Model;

use AppBundle\Entity\Answer;

class AnswerModel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    public $answer;

    /**
     * @var bool|null
     */
    public $helpful;

    /**
     * @var QuestionModel
     */
    public $question;

    /**
     * @var VetModel
     */
    public $vet;

    /**
     * @param Answer $answer
     * @param bool $deep
     */
    public function __construct(Answer $answer = null, bool $deep = true)
    {
        if ($answer) {
            $this->id = $answer->getId();
            $this->answer = $answer->getAnswer();
            $this->helpful = $answer->getHelpful();
            if ($deep) {
                $this->question = new QuestionModel($answer->getQuestion(), false);
            }
            $this->vet = new VetModel($answer->getVet());
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
