<?php

namespace AppBundle\Model;

use AppBundle\Entity\Vet;

class VetModel extends UserModel
{
    /**
     * @var string
     */
    public $specialty;

    /**
     * @var string
     */
    public $bio;

    /**
     * @param Vet $vet
     */
    public function __construct(Vet $vet = null)
    {
        if ($vet) {
            $this->specialty = $vet->getSpecialty();
            $this->bio = $vet->getBio();

            $user = $vet->getUser();
            parent::__construct($user);
        }
        parent::__construct(null);
    }

    public function updateVet(Vet $vet)
    {
        $vet->setSpecialty($this->specialty);
        $vet->setBio($this->bio);
        $this->updateUser($vet->getUser());
    }
}
