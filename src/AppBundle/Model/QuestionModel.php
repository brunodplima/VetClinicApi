<?php

namespace AppBundle\Model;

use AppBundle\Entity\Question;

class QuestionModel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    public $question;

    /**
     * @var int
     */
    public $status = Question::STATUS_PUBLIC;

    /**
     * @var array
     */
    public $answers = [];

    /**
     * @var UserModel()
     */
    public $user;

    /**
     * @param Question|null $question
     * @param bool $deep
     */
    public function __construct(Question $question = null, bool $deep = true)
    {
        if ($question) {
            $this->id = $question->getId();
            $this->question = $question->getQuestion();
            $this->status = $question->getStatus();
            if ($deep) {
                $answers = $question->getAnswers();
                foreach ($answers as $answer) {
                    $this->answers[] = new AnswerModel($answer, false);
                }
            }
            $this->user = new UserModel($question->getUser());
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
