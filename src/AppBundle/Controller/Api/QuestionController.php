<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Question;
use AppBundle\Entity\User;
use AppBundle\Form\QuestionType;
use AppBundle\Model\QuestionModel;
use AppBundle\Service\QuestionService;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/questions")
 */
class QuestionController extends BaseController
{
    /**
     * @Route("", name="api_question_index", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Retorna a lista completa de perguntas"
     * )
     * @param QuestionService $questionService
     * @return Response
     */
    public function indexAction(QuestionService $questionService)
    {
        return $this->createApiResponse(['data' => $questionService->getAll()]);
    }

    /**
     * @Route("/{question}", name="api_question_show", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Retorna uma pergunta específica"
     * )
     * @param Question $question
     * @return Response
     */
    public function showAction(Question $question)
    {
        // TODO: Permitir visualização sem autenticação, caso seja pública
        return $this->createApiResponse(new QuestionModel($question));
    }

    /**
     * @Route("/users/{user}", name="api_question_user", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Retorna as perguntas feitas pelo usuário ou veterinário especificado"
     * )
     * @param User $user
     * @param QuestionService $questionService
     * @return Response
     */
    public function userAction(User $user, QuestionService $questionService)
    {
        return $this->createApiResponse(['data' => $questionService->getAllByUser($user, $user !== $this->getUser())]);
    }

    /**
     * @Route("", name="api_question_create", methods={"POST"})
     * @SWG\Response(
     *     response=201,
     *     description="Cria uma pergunta"
     * )
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="JSON Payload",
     *     required=true,
     *     format="application/json",
     *     @SWG\Schema(
     *         type="object",
     *         required={"question", "status"},
     *         @SWG\Property(property="question", type="string", example="Quantas raças de cachorro existem?"),
     *         @SWG\Property(property="status", type="int", example="0"),
     *     )
     * )
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request)
    {
        $question = new Question();
        $form = $this->createForm(QuestionType::class, $question);
        $this->validateForm($form, $request);

        $em = $this->getDoctrine()->getManager();
        $question->setUser($this->getUser());
        $em->persist($question);
        $em->flush();

        return $this->createApiResponse(new QuestionModel($question), Response::HTTP_CREATED);
    }
}
