<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\User;
use AppBundle\Entity\Vet;
use AppBundle\Form\UserType;
use AppBundle\Form\VetModelType;
use AppBundle\Model\UserModel;
use AppBundle\Model\VetModel;
use AppBundle\Service\QuestionService;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends BaseController
{
    /**
     * @Route("me", name="api_user_me_show", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Retorna as informações de perfil do usuário ou veterinário logado"
     * )
     * @return Response
     */
    public function meShowAction()
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($vet = $user->getVet()) {
            return $this->createApiResponse(new VetModel($vet));
        }
        return $this->createApiResponse(new UserModel($user));
    }

    /**
     * @Route("me", name="api_user_me_update", methods={"PATCH", "PUT"})
     * @SWG\Response(
     *     response=200,
     *     description="Atualiza as informações de perfil do usuário ou veterinário logado"
     * )
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="JSON Payload",
     *     required=true,
     *     format="application/json",
     *     @SWG\Schema(
     *         type="object",
     *         required={"firstName", "lastName"},
     *         @SWG\Property(property="firstName", type="string", example="John"),
     *         @SWG\Property(property="lastName", type="string", example="Doe"),
     *         @SWG\Property(property="specialty", type="string", example="(Apenas Vet) Lorem Ipsum"),
     *         @SWG\Property(property="bio", type="string", example="(Apenas Vet) Lorem Ipsum sit amet"),
     *     )
     * )
     * @param Request $request
     * @return Response
     */
    public function meUpdateAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        /** @var Vet $vet */
        $vet = $user->getVet();
        $isVet = null !== $vet;

        $em= $this->getDoctrine()->getManager();
        $model = $isVet
            ? $this->handleVetModelForm($request, $vet)
            : $this->handleUserForm($request, $user);
        $em->persist($user);
        $em->flush();
        return $this->createApiResponse($model);
    }

    /**
     * @Route("me/questions", name="api_user_me_questions", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Retorna as perguntas feitas pelo usuário ou veterinário logado"
     * )
     * @param QuestionService $questionService
     * @return Response
     */
    public function userAction(QuestionService $questionService)
    {
        return $this->createApiResponse(['data' => $questionService->getAllByUser($this->getUser(), false)]);
    }

    private function handleUserForm(Request $request, User $user)
    {
        $form = $this->createForm(UserType::class, $user);
        $this->validateForm($form, $request);
        return new UserModel($user);
    }

    private function handleVetModelForm(Request $request, Vet $vet)
    {
        $vetModel = new VetModel($vet);
        $form = $this->createForm(VetModelType::class, $vetModel);
        $this->validateForm($form, $request);
        $vetModel->updateVet($vet);
        return $vetModel;
    }
}
