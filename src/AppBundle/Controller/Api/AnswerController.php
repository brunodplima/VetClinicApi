<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Answer;
use AppBundle\Form\AnswerType;
use AppBundle\Model\AnswerModel;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/answers")
 */
class AnswerController extends BaseController
{
    /**
     * @Route("", name="api_answer_create", methods={"POST"})
     * @SWG\Response(
     *     response=201,
     *     description="Cria uma resposta"
     * )
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="JSON Payload",
     *     required=true,
     *     format="application/json",
     *     @SWG\Schema(
     *         type="object",
     *         required={"question", "status"},
     *         @SWG\Property(property="question", type="int", example="1"),
     *         @SWG\Property(property="answer", type="string", example="NaN"),
     *     )
     * )
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request)
    {
        $vet = $this->getUser()->getVet();
        if (!$vet) {
            $this->throwApiException(['Apenas especialistas podem escrever uma resposta'], Response::HTTP_FORBIDDEN);
        }

        $answer = new Answer();
        $form = $this->createForm(AnswerType::class, $answer);
        $this->validateForm($form, $request);

        $em = $this->getDoctrine()->getManager();
        $answer->setVet($vet);
        $em->persist($answer);
        $em->flush();

        return $this->createApiResponse(new AnswerModel($answer), Response::HTTP_CREATED);
    }

    /**
     * @Route("/{answer}/helpful", name="api_answer_mark_as_helpful", methods={"POST"})
     * @SWG\Response(
     *     response=200,
     *     description="Marca uma resposta como sendo de ajuda"
     * )
     * @param Answer $answer
     * @return Response
     */
    public function markAsHelpfulAction(Answer $answer)
    {
        $vet = $this->getUser()->getVet();
        if ($vet) {
            $this->throwApiException(
                ['Apenas usuários podem considerar uma resposta como sendo de ajuda'],
                Response::HTTP_FORBIDDEN
            );
        }

        $em = $this->getDoctrine()->getManager();
        $answer->setHelpful(true);
        $em->persist($answer);
        $em->flush();

        return $this->createApiResponse(new AnswerModel($answer));
    }
}
